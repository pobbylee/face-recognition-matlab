%% CreateDatabase(TrainDatabasePath): function reshapes 2D images in 1D column vector
function T = CreateDatabase(trainDatabasePath)
% Description: reshapes all 2D images of the training database into 1D column vectors. Then, it puts these 1D column vectors in a row to 
% construct 2D matrix 'T'.

% Inputs:     trainDatabasePath: path of the images in "TrainingDatabase" folder created
% using faceDetects() function

% Returns:      T                      - A 2D matrix, containing all 1D image vectors. 'T' will be a MNxP 2D matrix.
    
    trainFiles = dir(trainDatabasePath);   
    trainNumber = 0;

    for i = 3:size(trainFiles, 1)
        trainNumber = trainNumber + 1;     % Number of all images in the training database
    end

%%%%%%%%%%%%%%%%%%%%%%%% Construction of 2D matrix from 1D image vectors
    T = [];
    
    for i = 1 : trainNumber
        str = int2str(i);       %convert 'i' from int to string
        str = strcat(str, '.jpg');   %str will be like 'i.jpg'
        str = strcat(trainDatabasePath, str);
    
        img = imread(str);      %read image
        
        [irow icol] = size(img);
   
        temp = reshape(img', irow*icol, 1);   % Reshaping 2D images into 1D image vectors
        T = [T temp];   % 'T' grows after each iteration
    end
    
end  %end of CreateDatabase()
