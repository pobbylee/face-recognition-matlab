%% EigenfaceCore(T): calculates eigen faces using PCA, plots cumulative curve and plots the 1st eigen face
function [meanImage, A, Eigenfaces] = EigenfaceCore(T)

% Inputs:      T                      - A 2D matrix, containing all 1D image vectors.
%                                       length of 'T' will be a MNxP 2D matrix.
%                                       T: created using 'CreateDatabase.m'
% 
% Returns:       mean                   - (M*Nx1) Mean of the training database
%                Eigenfaces             - (M*Nx(P-1)) Eigen vectors of the covariance matrix of the training database
%                A                      - (M*NxP) Matrix of centered image vectors
%

    %% Calculating the mean image 
    meanImage = mean(T,2);   % Computing the average face image m = (1/P)*sum(Tj's)    (j = 1 : P)
    trainNumber = size(T,2);

    %% Calculating the deviation of each image from mean image
    % Number of training images is less than the number of pixels (M*N), the most 
    %non-zero eigenvalues that can be found are equal to P-1. 
    % We can calculate eigenvalues of A'*A (a PxP matrix) instead of
    % A*A' (a M*NxM*N matrix).So the dimensionality will decrease.

    A = [];  
    for i = 1 : trainNumber
        temp = double(T(:,i)) - meanImage; % subtract mean from all images in T
        A = [A temp];       % Merging all centered images
    end

    L = A'*A;       % L is the replaced matrix of covariance matrix C = A*A'.
    [V D] = eig(L);  % Diagonal elements of D are the eigenvalues, V: eigen vectors
    
    %% Sorting and eliminating eigenvalues
    % All eigenvalues of matrix L are sorted and those who are less than a
    % specified threshold, are eliminated. 
    eigenValuesSorted = sort(diag(D), 'descend');   %sorting eigenvalues descending 200*1
    
    %choosing 170 eigenvalues and fetching their eigenvectors
    L_eig_vec = [];
    for i = 1 : size(V,2) 
        if(eigenValuesSorted(i,1) > eigenValuesSorted(171,1))
            L_eig_vec = [L_eig_vec V(:,i)];
        end
    end
    
    L_eig_vec = norm(L_eig_vec);    %normalize chosen eigenvectors
    
    %% Calculating the eigenvectors of covariance matrix 'C'
    % Eigenvectors of covariance matrix C (or so-called "Eigenfaces")
    % can be recovered from L's eiegnvectors.
    Eigenfaces = A * L_eig_vec; % A: centered image vectors
    
    %show the first eigenface as grayscale image
    firstEigenFace = reshape(Eigenfaces(:,1),162,193)';
    figure(1), imagesc(firstEigenFace); colormap(gray);
    title('First Eigen Face');
    
    %show cumulative curve
    eigenValues = [];
	for i = 1 : 170      %170: as we selected 170 eigenvalues
        eigenValues = [eigenValues eigenValuesSorted(i,1)];     %1*170
    end
    
	TotalEigenValue = sum(eigenValues);
	eigenNormed = eigenValues./TotalEigenValue;   %normalization
	Cummulative = cumsum(eigenNormed);            %cumulative 
	[r, c]= size(Cummulative);
    
    figure(2);
    plot(0:1:c-1, Cummulative, 'r');
    title('Cummulative Curve');

end   %end of EigenFaceCore()
